/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef GEOGEOMETRYPLUGINLOADER_H_
#define GEOGEOMETRYPLUGINLOADER_H_
#include "GeoModelKernel/GeoPluginLoader.h"
#include "GeoModelKernel/GeoVGeometryPlugin.h"
typedef GeoPluginLoader<GeoVGeometryPlugin> GeoGeometryPluginLoader;
#endif
